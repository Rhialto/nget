AC_DEFUN([AC_PROTOTYPE_RECV],[
AC_PROTOTYPE(recv,
 [
  #include <sys/types.h>
#ifdef WIN32
  #include <winsock.h>
#else
  #include <sys/socket.h>
#endif
 ],
 [
  ARG2 b = 0;
  recv(0, b, 0, 0);
 ],
 ARG2, [const void*, const char*, void*, char*])
])

