dnl
dnl AC_PROTOTYPE_EACH(tag, values)
dnl
dnl Call AC_PROTOTYPE_LOOP for each values and define the macro tag_VAL to
dnl the current value.
dnl
AC_DEFUN([AC_PROTOTYPE_EACH],[
  ifelse($2,, [
  ], [
    pushdef([$1_VAL], $2)
    AC_PROTOTYPE_LOOP(rest)
    popdef([$1_VAL])
    AC_PROTOTYPE_EACH($1, m4_shift(m4_shift($@)))
  ])
])
