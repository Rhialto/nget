dnl @synopsis AC_PROTOTYPE(function, includes, code, TAG1, values1 [, TAG2, values2 [...]])
dnl
dnl Try all the combinations of <TAG1>, <TAG2>... to successfully compile <code>.
dnl <TAG1>, <TAG2>, ... are substituted in <code> and <include> with values found in
dnl <values1>, <values2>, ... respectively. <values1>, <values2>, ... contain a list of
dnl possible values for each corresponding tag and all combinations are tested.
dnl When AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[include]], [[code]])],[],[]) is successfull for a given substitution, the macro
dnl stops and defines the following macros: FUNCTION_TAG1, FUNCTION_TAG2, ... using AC_DEFINE()
dnl with values set to the current values of <TAG1>, <TAG2>, ...
dnl If no combination is successfull the configure script is aborted with a message.
dnl
dnl Intended purpose is to find which combination of argument types is acceptable for a
dnl given function <function>. It is recommended to list the most specific types first.
dnl For instance ARG1, [size_t, int] instead of ARG1, [int, size_t].
dnl
dnl Generic usage pattern:
dnl
dnl 1) add a call in configure.in
dnl
dnl  AC_PROTOTYPE(...)
dnl
dnl 2) call autoheader to see which symbols are not covered
dnl
dnl 3) add the lines in acconfig.h
dnl
dnl  /* Type of Nth argument of function */
dnl  #undef FUNCTION_ARGN
dnl
dnl 4) Within the code use FUNCTION_ARGN instead of an hardwired type
dnl
dnl Complete example:
dnl
dnl 1) configure.in
dnl
dnl  AC_PROTOTYPE(getpeername,
dnl  [
dnl   #include <sys/types.h>
dnl   #include <sys/socket.h>
dnl  ],
dnl  [
dnl   int a = 0;
dnl   ARG2 * b = 0;
dnl   ARG3 * c = 0;
dnl   getpeername(a, b, c);
dnl  ],
dnl  ARG2, [struct sockaddr, void],
dnl  ARG3, [socklen_t, size_t, int, unsigned int, long unsigned int])
dnl
dnl 2) call autoheader
dnl
dnl  autoheader: Symbol `GETPEERNAME_ARG2' is not covered by ./acconfig.h
dnl  autoheader: Symbol `GETPEERNAME_ARG3' is not covered by ./acconfig.h
dnl
dnl 3) acconfig.h
dnl
dnl  /* Type of second argument of getpeername */
dnl  #undef GETPEERNAME_ARG2
dnl
dnl  /* Type of third argument of getpeername */
dnl  #undef GETPEERNAME_ARG3
dnl
dnl 4) in the code
dnl      ...
dnl      GETPEERNAME_ARG2 name;
dnl      GETPEERNAME_ARG3 namelen;
dnl      ...
dnl      ret = getpeername(socket, &name, &namelen);
dnl      ...
dnl
dnl Implementation notes: generating all possible permutations of
dnl the arguments is not easily done with the usual mixture of shell and m4,
dnl that is why this macro is almost 100% m4 code. It generates long but simple
dnl to read code.
dnl
dnl @version $Id: ac_prototype.m4,v 1.2 2000/08/11 06:28:24 simons Exp $
dnl @author Loic Dachary <loic@senga.org>
dnl

AC_DEFUN([AC_PROTOTYPE],[
dnl
dnl Upper case function name
dnl
 pushdef([function],translit([$1], [a-z], [A-Z]))
dnl
dnl Collect tags that will be substituted
dnl
 pushdef([tags],[AC_PROTOTYPE_TAGS(m4_shift(m4_shift(m4_shift($@))))])
dnl
dnl Wrap in a 1 time loop, when a combination is found break to stop the combinatory exploration
dnl
 for i in 1
 do
   AC_PROTOTYPE_LOOP(AC_PROTOTYPE_REVERSE($1, AC_PROTOTYPE_SUBST($2,tags),AC_PROTOTYPE_SUBST($3,tags),m4_shift(m4_shift(m4_shift($@)))))
   AC_MSG_ERROR($1 unable to find a working combination)
 done
 popdef([tags])
 popdef([function])
])
