AC_DEFUN([MY_SEARCH_LIBS],[
AC_CACHE_CHECK([for $5], [my_cv_$1],
[ac_func_search_save_LIBS=$LIBS
my_cv_$1=no
AC_LINK_IFELSE([AC_LANG_PROGRAM([[$2]], [[$3]])],[my_cv_$1="none required"],[])
if test "$my_cv_$1" = no; then
  for ac_lib in $4; do
    LIBS="-l$ac_lib $ac_func_search_save_LIBS"
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[$2]], [[$3]])],[my_cv_$1="-l$ac_lib"
break],[])
  done
fi
LIBS=$ac_func_search_save_LIBS])
AS_IF([test "$my_cv_$1" != no],
  [test "$my_cv_$1" = "none required" || LIBS="$my_cv_$1 $LIBS"
  AC_DEFINE(HAVE_[]translit([$1], [a-z], [A-Z]),1,[Do we have $5?])])dnl
])
