AC_DEFUN(MY_CHECK_POPT_CONST,
[AC_CACHE_CHECK([if popt wants const argvs],
  ac_cv_popt_const_argv,
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[#include <popt.h>]], [[const char ** targv=NULL;poptContext c=poptGetContext(NULL,1,targv,NULL,0);]])],[ac_cv_popt_const_argv=yes],[ac_cv_popt_const_argv=no])])
if test $ac_cv_popt_const_argv = yes; then
  AC_DEFINE(POPT_CONST_ARGV,1,[Does popt want const argvs?])
fi
])

