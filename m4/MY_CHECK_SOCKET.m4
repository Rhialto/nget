AC_DEFUN([MY_CHECK_SOCKET],[MY_SEARCH_LIBS(socket,
[#include <sys/types.h>
#ifdef HAVE_WINSOCK_H
#include <winsock.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif],
[socket(AF_INET, SOCK_STREAM, 0);],
[socket ws2_32 wsock32],
[library containing socket])
])
