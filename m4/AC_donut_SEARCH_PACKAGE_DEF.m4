dnl package that defaults to enabled
AC_DEFUN([AC_donut_SEARCH_PACKAGE_DEF],
[
AC_ARG_WITH($1,
AS_HELP_STRING([--without-$1],[disables $1 usage completely])
AS_HELP_STRING([--with-$1=DIR],[look in DIR for $1]),
with_$1=$withval
,
with_$1=yes
)
AC_donut_SEARCH_PACKAGE_sub([$1], [$2], [$3], [$4], [$5], [$6])
]
)
