AC_DEFUN([MY_TRY_COMPILE_HASH_MAP],[
	AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
#ifdef HAVE_HASH_MAP
#include <hash_map>
#elif HAVE_EXT_HASH_MAP
#include <ext/hash_map>
#elif HAVE_HASH_MAP_H
#include <hash_map.h>
#endif
using namespace std;
$1
	]], [[
	hash_map<int, long> h;
	hash_multimap<int, long> h2;
	]])],[$2],[$3
	])
])
