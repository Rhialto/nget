dnl Note:
dnl =====
dnl I have not implemented the following suggestion because I don't have
dnl access to such a broken environment to test the macro.  So I'm just
dnl appending the comments here in case you have, and want to fix
dnl AC_FUNC_MKDIR that way.
dnl
dnl |Thomas E. Dickey (dickey@herndon4.his.com) said:
dnl |  it doesn't cover the problem areas (compilers that mistreat mkdir
dnl |  may prototype it in dir.h and dirent.h, for instance).
dnl |
dnl |Alexandre:
dnl |  Would it be sufficient to check for these headers and #include
dnl |  them in the AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[]], [[]])],[],[]) block?  (and is AC_HEADER_DIRENT
dnl |  suitable for this?)
dnl |
dnl |Thomas:
dnl |  I think that might be a good starting point (with the set of recommended
dnl |  ifdef's and includes for AC_HEADER_DIRENT, of course).


dnl @synopsis AC_donut_CHECK_PACKAGE(PACKAGE, FUNCTION, LIBRARY, HEADERFILE [, ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND]])
dnl
dnl Provides --with-PACKAGE, --with-PACKAGE-include and --with-PACKAGE-lib
dnl options to configure. Supports --with-PACKAGE=DIR approach which looks
dnl first in DIR and then in DIR/{include,lib} but also allows the include
dnl and lib directories to be specified seperately.
dnl
dnl adds the extra -Ipath to CFLAGS if needed
dnl adds extra -Lpath to LD_FLAGS if needed
dnl searches for the FUNCTION in the LIBRARY with
dnl AC_CHECK_LIBRARY and thus adds the lib to LIBS
dnl
dnl defines HAVE_PKG_PACKAGE if it is found, (where PACKAGE in the
dnl HAVE_PKG_PACKAGE is replaced with the actual first parameter passed)
dnl
dnl Based on:
dnl @version $Id: ac_caolan_check_package.m4,v 1.5 2000/08/30 08:50:25 simons Exp $
dnl @author Caolan McNamara <caolan@skynet.ie> with fixes from Alexandre Duret-Lutz <duret_g@lrde.epita.fr>.
dnl
AC_DEFUN([AC_donut_CHECK_PACKAGE_pre],
[
AC_ARG_WITH($1-prefix,
[AS_HELP_STRING([--with-$1-prefix=DIR],[use $1 and look in DIR/{include,lib}/])],
if test "$withval" != no; then
	with_$1=yes
	if test "$withval" != yes; then
		$1_include="${withval}/include"
		$1_libdir="${withval}/lib"
	fi
fi
)

AC_ARG_WITH($1-include,
[AS_HELP_STRING([--with-$1-include=DIR],[specify exact include dir for $1 headers])],
$1_include="$withval")

AC_ARG_WITH($1-lib,
[AS_HELP_STRING([--with-$1-lib=DIR],[specify exact library dir for $1 library])],
$1_libdir="$withval")

if test "${with_$1}" != no ; then
        OLD_LIBS=$LIBS
        OLD_LDFLAGS=$LDFLAGS
        OLD_CFLAGS=$CFLAGS
        OLD_CPPFLAGS=$CPPFLAGS

        if test "${$1_libdir}" ; then
                LDFLAGS="$LDFLAGS -L${$1_libdir}"
	elif test "${with_$1}" != yes ; then
                LDFLAGS="$LDFLAGS -L${with_$1}"
        fi
        if test "${$1_include}" ; then
                CPPFLAGS="$CPPFLAGS -I${$1_include}"
                CFLAGS="$CFLAGS -I${$1_include}"
	elif test "${with_$1}" != yes ; then
                CPPFLAGS="$CPPFLAGS -I${with_$1}"
                CFLAGS="$CFLAGS -I${with_$1}"
        fi

	no_good=no
                
	ifelse([$7], , , [$7])

        if test "$no_good" = yes; then
dnl     broken
                LIBS=$OLD_LIBS
                LDFLAGS=$OLD_LDFLAGS
                CPPFLAGS=$OLD_CPPFLAGS
                CFLAGS=$OLD_CFLAGS
		
                ifelse([$6], , , [$6])
        else
dnl     fixed
                ifelse([$5], , , [$5])

                AC_DEFINE(HAVE_PKG_$1,1,[Define if you have $3 and $4])
        fi

else
        dnl pointless, but sh barfs if the else..fi is empty (when arg 6 is not used)
        no_good=yes 

        ifelse([$6], , , [$6])

fi

])
