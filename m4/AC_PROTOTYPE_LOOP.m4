dnl
dnl AC_PROTOTYPE_LOOP([tag, values, [tag, values ...]], code, include, function)
dnl
dnl If there is a tag/values pair, call AC_PROTOTYPE_EACH with it.
dnl If there is no tag/values pair left, tries to compile the code and include
dnl using AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[]], [[]])],[],[]). If it compiles, AC_DEFINE all the tags to their
dnl current value and exit with success.
dnl
AC_DEFUN([AC_PROTOTYPE_LOOP],[
 ifelse(m4_eval($# > 3), 1,
   [
     pushdef([rest],[m4_shift(m4_shift($@))])
     AC_PROTOTYPE_EACH($2,$1)
     popdef([rest])
   ], [
     AC_MSG_CHECKING($3 AC_PROTOTYPE_STATUS(tags))
dnl
dnl Activate fatal warnings if possible, gives better guess
dnl
     ac_save_CPPFLAGS="$CPPFLAGS"
	 dnl nget is c++, so we can hardcode this since autoconf barfs on the tests
     if test "$GXX" = "yes" ; then CPPFLAGS="$CPPFLAGS -Werror" ; fi
dnl     ifelse(AC_LANG,CPLUSPLUS,if test "$GXX" = "yes" ; then CPPFLAGS="$CPPFLAGS -Werror" ; fi)
dnl     ifelse(AC_LANG,C,if test "$GCC" = "yes" ; then CPPFLAGS="$CPPFLAGS -Werror" ; fi)
     AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[$2]], [$1])],[
      CPPFLAGS="$ac_save_CPPFLAGS"
      AC_MSG_RESULT(ok)
      AC_PROTOTYPE_DEFINES(tags)
      break;
     ],[
      CPPFLAGS="$ac_save_CPPFLAGS"
      AC_MSG_RESULT(not ok)
     ])
   ]
 )
])
