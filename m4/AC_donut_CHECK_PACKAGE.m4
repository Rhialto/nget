dnl package that defaults to disabled
AC_DEFUN([AC_donut_CHECK_PACKAGE],
[
AC_ARG_WITH($1,
[AS_HELP_STRING([--with-$1(=DIR)],[use $1, optionally looking in DIR])],
with_$1=$withval
,
with_$1=no
)
AC_donut_CHECK_PACKAGE_sub([$1], [$2], [$3], [$4], [$5], [$6])
]
)
