AC_DEFUN([AC_donut_CHECK_PACKAGE_sub],
[
	AC_donut_CHECK_PACKAGE_pre([$1], [$2], [$3], [$4], [$5], [$6], [
        AC_CHECK_LIB($3,$2,,no_good=yes)
        AC_CHECK_HEADERS($4,,no_good=yes)
	])
])
