dnl
dnl AC_PROTOTYPE_SUBST(string, tag)
dnl
dnl Substitute all occurence of <tag> in <string> with <tag>_VAL.
dnl Assumes that tag_VAL is a macro containing the value associated to tag.
dnl
AC_DEFUN([AC_PROTOTYPE_SUBST],[ifelse($2,,[$1],[AC_PROTOTYPE_SUBST(patsubst([$1],[$2],[$2[]_VAL]),m4_shift(m4_shift($@)))])])
