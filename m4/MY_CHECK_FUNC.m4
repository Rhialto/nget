# MY_CHECK_FUNC(FUNCTION, [ARGS], [ACTION-IF-FOUND], [ACTION-IF-NOT-FOUND], [HEADERS])
# -----------------------------------------------------------------
AC_DEFUN([MY_CHECK_FUNC],
[AS_VAR_PUSHDEF([ac_var], [ac_cv_func_$1])dnl
AC_CACHE_CHECK([for $1], ac_var,
[AC_LINK_IFELSE([AC_LANG_PROGRAM([$5],[$1($2)])],
		[AS_VAR_SET(ac_var, yes)],
		[AS_VAR_SET(ac_var, no)])])
AS_IF([test AS_VAR_GET(ac_var) = yes], [$3], [$4])dnl
AS_VAR_POPDEF([ac_var])dnl
])# MY_CHECK_FUNC

