# MY_CHECK_FUNCS(FUNCTION..., [ARGS], [ACTION-IF-FOUND], [ACTION-IF-NOT-FOUND], [HEADERS])
# ---------------------------------------------------------------------
AC_DEFUN([MY_CHECK_FUNCS],
[m4_foreach_w([AC_Func],[$1],[AH_TEMPLATE(AS_TR_CPP(HAVE_[]AC_Func),
	       [Define to 1 if you have the `]AC_Func[' function.])])dnl
for ac_func in $1
do
MY_CHECK_FUNC($ac_func,[$2],
	      [AC_DEFINE_UNQUOTED(AS_TR_CPP([HAVE_$ac_func])) $3],
	      [$4],[$5])
done
])
