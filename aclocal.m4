# generated automatically by aclocal 1.16.5 -*- Autoconf -*-

# Copyright (C) 1996-2021 Free Software Foundation, Inc.

# This file is free software; the Free Software Foundation
# gives unlimited permission to copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.

m4_ifndef([AC_CONFIG_MACRO_DIRS], [m4_defun([_AM_CONFIG_MACRO_DIRS], [])m4_defun([AC_CONFIG_MACRO_DIRS], [_AM_CONFIG_MACRO_DIRS($@)])])
m4_include([m4/AC_FUNC_MKDIR.m4])
m4_include([m4/AC_PROTOTYPE.m4])
m4_include([m4/AC_PROTOTYPE_DEFINES.m4])
m4_include([m4/AC_PROTOTYPE_EACH.m4])
m4_include([m4/AC_PROTOTYPE_LOOP.m4])
m4_include([m4/AC_PROTOTYPE_RECV.m4])
m4_include([m4/AC_PROTOTYPE_REVERSE.m4])
m4_include([m4/AC_PROTOTYPE_STATUS.m4])
m4_include([m4/AC_PROTOTYPE_SUBST.m4])
m4_include([m4/AC_PROTOTYPE_TAGS.m4])
m4_include([m4/AC_donut_CHECK_PACKAGE.m4])
m4_include([m4/AC_donut_CHECK_PACKAGE_DEF.m4])
m4_include([m4/AC_donut_CHECK_PACKAGE_pre.m4])
m4_include([m4/AC_donut_CHECK_PACKAGE_sub.m4])
m4_include([m4/AC_donut_SEARCH_PACKAGE_DEF.m4])
m4_include([m4/AC_donut_SEARCH_PACKAGE_sub.m4])
m4_include([m4/MY_CHECK_FUNC.m4])
m4_include([m4/MY_CHECK_FUNCS.m4])
m4_include([m4/MY_CHECK_POPT_CONST.m4])
m4_include([m4/MY_CHECK_SOCKET.m4])
m4_include([m4/MY_CHECK_TERMSTUFF.m4])
m4_include([m4/MY_DISABLE_OPT.m4])
m4_include([m4/MY_SEARCH_LIBS.m4])
m4_include([m4/SOCK_CHECK_TYPE.m4])
